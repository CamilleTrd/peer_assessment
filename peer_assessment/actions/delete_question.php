<?php
/**
*
* Action : delete_question from model in parent group
* @package peer_assessment 
*
**/

$question_guid = get_input('question_guid');

delete_entity($question_guid, true);

//forward to group profile page 
system_message(elgg_echo('peer_assessment:model:question_delete:success'));
forward(REFERER);
	