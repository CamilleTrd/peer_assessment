<?php
/**
*
* Action : Launch peer assessment process 
* Random association of subgroups and 
* creation of sets for eveluation and form from model in parent group
* 
* @package peer_assessment
*
**/

//get current group id
$group_guid = get_input('group_guid');
$started = pa_is_started($group_guid);

if($started){
	register_error(elgg_echo('peer_assessment:start:failure:already_started'));
	forward(REFERER);
}else{

	$model = get_peer_assessment_entity_by_container($group_guid, 'model_peer_assessment');
	
	if(empty($model)){
		// no model exist !! can't start peer assessment
		register_error(elgg_echo('peer_assessment:start:failure:no_model'));
		forward(REFERER);
	}
	
	
	// get subgroups entities
	$subgroups_array = get_group_parent($group_guid, true);
	
	//has to have more than 1 subgroup
	if(count($subgroups_array)>1){
		
		//declare as started 
		$model->started = true;
		
		//Do shuffle on array before pairing.
		shuffle($subgroups_array);
		
		//retreive only guids in array
		$subgroups_array_guid = array();
		foreach ($subgroups_array as $subgrp){
			$subgroups_array_guid[] = $subgrp->guid;
		}
		
		$size_array = sizeof($subgroups_array_guid);
		$eval_array = array();
		foreach ($subgroups_array_guid as $key=>$subgrp_guid){
			//$subgrp pair with next in line -> evaluate_pa relationship
			//We define here that : $subgrp is evaluating $subgroups_array[$key+1]
			//store in $eval_array the couples for use later in this page. key = grp_guid evaluator.
			
			//if end of array grab first group
			if($key+1 >= $size_array){
				add_entity_relationship($subgrp_guid, 'evaluates_pa_'.$model->guid, $subgroups_array_guid[0]);
				$eval_array[$subgrp_guid]=$subgroups_array_guid[0];
			}else{
				add_entity_relationship($subgrp_guid, 'evaluates_pa_'.$model->guid, $subgroups_array_guid[$key+1]);
				$eval_array[$subgrp_guid]=$subgroups_array_guid[$key+1];
			}
			
		}
		
		//Create Forms and add them to groups
		// $eval_array contains only guids
		foreach($eval_array as $grp_eval_guid => $grp_assessee_guid){
			
			//create set in sub-group being evaluated
			$set = create_evaluation_set($grp_assessee_guid, $grp_eval_guid);
			//create form in sub-group evaluator
			create_form($grp_eval_guid, $set, $model);
			
		}
		
		
		system_message(elgg_echo('peer_assessment:start:success'));
		//return to parent page
		forward("/peer_assessment/group/{$group_guid}/parent");
	}else{
		register_error(elgg_echo('peer_assessment:start:error:one_subgrp'));
		//return to parent page
		forward("/peer_assessment/group/{$group_guid}/parent");
	}
}
	
	
	
