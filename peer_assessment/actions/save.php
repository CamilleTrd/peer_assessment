<?php
/**
*
* Action : save form in subgroup
* @package peer_assessment 
*
**/

$group_guid = get_input('group_guid');
$model_guid = get_input('model_guid');
$form_guid = get_input('form_guid');

$group = get_entity($group_guid);

//Retreive array of answers
$answers_texts = get_input('answers_texts');
$answers_guids = get_input('answers_guids');

//index in array is question guid
$array_questions = get_questions_by_model_guid($model_guid);


//save answer for each question of form
foreach($array_questions as $key => $question){
	
	//save answers. 
	//object has been created during form creation (cf. lib)
	$answer = get_entity($answers_guids[$key]);
	
	if ($answer->canEdit(elgg_get_logged_in_user_guid())){
		$answer->description = $answers_texts[$key];
		$answer->save();
	}else{
		register_error(elgg_echo('peer_assessment:from:save:error_cannot_edit'));	
		forward(REFERER);
	}
	
}

//forward to group profile page 
system_message(elgg_echo('peer_assessment:form:save:success'));
forward(REFERER);
	