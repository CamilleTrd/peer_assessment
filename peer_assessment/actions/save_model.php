<?php
/**
*
* Action : save model form from parent group
* @ peer_assessment package
*
**/


$loggedinuser = elgg_get_logged_in_user_guid();

//retreive values 

//model guid
$model_guid = get_input('model_guid');

// we are creating a new model if no model guid is return by the form
$editing = false;
if(!empty($model_guid)){
	$editing = true;
}
$creating = !$editing;


//group guid
$container_guid = get_input('container_guid');
$container = get_entity($container_guid);

$title = get_input('model-title');
$description = get_input('model-description');

$array_question_guids = get_input('question-guids');
$array_question_titles = get_input('question-titles');
$array_question_details = get_input('question-details');

//$access_id = get_input('access_id', ACCESS_DEFAULT);

elgg_make_sticky_form('model');

$isgrpadmin = is_group_admin($container, $loggedinuser);

//check if allowed editing of model to current user
if ($creating && !$isgrpadmin) {
	register_error(elgg_echo('peer_assessment:model:failure:permissiondenied:creating'));
	forward(REFERER);
} elseif ($editing && !$isgrpadmin) {
	register_error(elgg_echo('peer_assessment:model:failure:permissiondenied:editing'));
	forward(REFERER);
}

// check if title, question_1_title and array_question_title are empty before creating the object model
if (empty($title)) {
	register_error(elgg_echo('peer_assessment:model:blank_title'));
	forward(REFERER);
}
if (empty($array_question_titles) || empty($array_question_titles[0])) {
	register_error(elgg_echo('peer_assessment:model:blank_title_question'));
	forward(REFERER);
}

//create objects
if($creating){
		
	$model = new ElggObject();
	$model->container_guid = $container_guid;
	$model->access_id = $container->group_acl;
	$model->owner_guid = $container_guid;
	$model->subtype = "model_peer_assessment";
	
}else {
	$model = get_entity($model_guid);
}

$model->title = $title;
$model->description = $description;
$model->save();


// retreive number of questions in model
$count = 0;
foreach ($array_question_titles as $question_title){
	//create question objects

	if($array_question_guids[$count]==0){
		$question = new ElggObject();
	}else {
		$question = get_entity($array_question_guids[$count]);
	}
	
	if(!empty($question_title)){
		$question->container_guid = $model->guid;
		$question->title = $question_title;
		$question->description = $array_question_details[$count];
		$question->access_id = $container->group_acl;
		$question->subtype = "question_peer_assessment";
		$question->save();
		
		$count ++;
	}else{
		break;	
	}
	
}

if ($model->save()) {
	elgg_clear_sticky_form('model');
	
	system_message(elgg_echo('peer_assessment:model:success_save'));
	forward(REFERER);
	
} else {
	register_error("peer_assessment:failure:permissiondenied");
	forward(REFERER);
}
