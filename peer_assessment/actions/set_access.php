<?php
/**
*
* Action : send form in subgroups to parent group
* No more edit allowed
*
* @ package peer_assessment
*
**/

$loggedin_user_guid = elgg_get_logged_in_user_guid();

//get current group id (sub-group)
$group_guid = get_input('group_guid');

//retreive sub groups
$subgroups_array = get_group_parent($group_guid, true);

foreach ($subgroups_array as $subgrp){
	$subgp_form = get_peer_assessment_entity_by_container($subgrp->guid, 'form_peer_assessment');
	
	//fix subgrp access value
	$subgrp->access_id = ACCESS_LOGGED_IN;
	$subgrp->save();
	
	//retreive set of evaluated sub group stored in corresponding form in evaluator subgrp
	$subgp_set = get_entity($subgp_form->set);
	
	if($subgp_set->canEdit($loggedin_user_guid)){
		$subgp_set->access_id = $subgp_form->set_collection_id;
		$subgp_set->save();
	}else{
		break;
		register_error(elgg_echo('peer_assessment:parent:set_access:fail_cantedit'));
		forward(REFERER);
	}
}


//redirect to form view page when finish send action
system_message(elgg_echo('peer_assessment:parent:set_access:success'));
forward(REFERER);