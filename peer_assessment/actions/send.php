<?php
/**
*
* Action : send form in subgroups to parent group
* No more edit allowed
*
* @ package peer_assessment
*
**/

//get current group id (sub-group)
$form_guid = get_input('form_guid');
$form = get_entity($form_guid);

if($form){
	//declare as sent and evaluation is finnished 
	$form->sent = true;
}

//send intern message to group aprent owner with link to view form finished.
$group_parent = get_group_parent($form->container_guid, false);
$group_parent_owner_guid = $group_parent->owner_guid;
$group = get_entity($form->owner_guid);

$url_list_pairs = $CONFIG->wwwroot."/peer_assessment/group/{$group_parent}/list_subgroups";
$subject = sprintf(elgg_echo('peer_assessment:form:message:subject'), $group->name);
$body  = sprintf(elgg_echo('peer_assessment:form:message:body'), $group->name, $form->getURL(), $url_list_pairs);
$to = $group_parent_owner_guid;
$from = $form->owner_guid;

notify_user($to, $from, $subject, $body);

//redirect to form view page when finish send action
system_message(elgg_echo('peer_assessment:form:sent:success'));
// go to model view page
$form_url_view = "/peer_assessment/group/{$form->owner_guid}/form_view";
forward($form_url_view);