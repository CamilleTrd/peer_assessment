<?php
/**
 * @package peer_assessment
 */

$french = array(

	"item:object:answer_peer_assessment"=>"Réponses (évaluation par les pairs)",
	"item:object:form_peer_assessment"=>"Formulaires (évaluation par les pairs)",
	"item:object:model_peer_assessment"=>"Modèles (évaluation par les pairs)",
	"item:object:question_peer_assessment"=>"Questions (évaluation par les pairs)",
	
	"peer_assessment:peer_assessment" => "Evaluation par les Pairs",
	'peer_assessment:enable_peer_assessment' => "Activer le module d'évaluation par les pairs ?",
	'peer_assessment:parent' => "Parent",
	'peer_assessment:parent:title' => "Page parent du module d'évaluation par les pairs",
	'peer_assessment:model' => "Modèle",
	'peer_assessment:form' => "Formulaire",
	
	"peer_assessment:failure:permissiondenied" => "Désolé. Vous n'avez pas les permissions nécessaires pour accèder à cette page.",
	
	// Group module
	'peer_assessment:title' => "Evaluation par les Pairs",
	'peer_assessment:group_module:none' => "Il n'existe aucun formulaire pour le moment.",
	
	"peer_assessment:peer_assessment_menu:group_parent" => "Gestion du module d'évaluation par les pairs",
	"peer_assessment:peer_assessment_menu:group" => "Formulaire d'évaluation par les pairs",
	
	//parent page
	"peer_assessment:parent:model_desc" => "Créer ou modifier le modèle",
	'peer_assessment:parent:model_button' => "Créer/Modifier",
	'peer_assessment:parent:model_button_view' => "Voir le modèle",
	"peer_assessment:parent:start_desc" => "Démarrer le module d'évaluation par les pairs. En cliquant sur le bouton 'démarrer', le module récupèrera la liste de tous les sous-groupes d'activités de ce groupe d'activités et les associera par paire de façon aléatoire. Un formulaire sera créé selon le modèle défini ici, ainsi qu'un cadre d'évaluation dans chaque sous-groupe d'activités. Il faut au moins deux sous-groupes d'activités pour pouvoir démarrer le module.",
	"peer_assessment:parent:start_desc_subtext" => "Merci de vérifier que tous les sous-groupes d'activités ont bien été créés. Si un nouveau sous-groupe d'activités est ajouté après que le module ait été démarré, il ne sera pas pris en compte pour l'évaluation. De plus, si un membre s'inscrit après que l'évaluation ait démarré, il ne pourra pas accéder à certains éléments nécessaires.",
	'peer_assessment:parent:start_button' => "Démarrer",
	'peer_assessment:parent:list_view_button' => "Voir les paires",
	'peer_assessment:parent:set_access_info' => "En cliquant sur le bouton 'Réparer les accès aux cadres', le système rétablira les droits en lecture pour tous les cadres des sous-groupes d'activités participants à l'évalution par les pairs en cours. Les cadres seront accessibles en lecture par les membres du groupe d'activités évaluateur et du groupe d'activités évalué.",
	'peer_assessment:parent:set_access_button' => "Réparer les accès aux cadres",
	'peer_assessment:parent:set_access:success' => "Les droit d'accès aux cadres des sous-groupes en lecture ont été rétablis.",
	'peer_assessment:parent:set_access:fail_cantedit' => "Vous n'avez pas les permissions nécessaires pour éditer les cadres des sous-groupes.",
	
	//Start - launch action
	'peer_assessment:start:failure:no_model' => "Désolé, vous ne pouvez pas démarrer le module d'évaluation par les pairs si vous n'avez pas créé de modèle. Merci de créer un modèle et de réessayer.",
	'peer_assessment:start:success' => "Le module d'évaluation par les pairs a démarré avec succès.",
	'peer_assessment:start:failure:already_started' => "Le module d'évaluation par les pairs a déjà été démarré",
		'peer_assessment:start:error:one_subgrp' => "Désolé, vous devez avoir au moins deux sous-groupe pour pouvoir démarrer le module.",
	
	//list of subgroups
	'peer_assessment:table:list' => "Liste des paires d'évaluation",
	'peer_assessment:table:group_evaluator' => "Groupe Evaluateur",
	'peer_assessment:table:evaluator_of' => "évalue >",
	'peer_assessment:table:group_evaluee' => "Groupe Evalué",
	
	//model (parent group)
	'peer_assessment:model:edit' => "Modifier le modèle d'évaluation par les pairs",
	'peer_assessment:model:view' => "Voir le modèle d'évaluation par les pairs",
	'peer_assessment:unknown_model' => "Le modèle d'évaluation par les pairs demandé n'existe pas ou vous n'avez pas les permissions nécessaires pour le modifier.",
	'peer_assessment:model:success_save' => "Le modèle a été enregistré ",
	'peer_assessment:model:blank_title' => "Le modèle doit avoir un titre",
	'peer_assessment:model:blank_title_question' => "Les questions du modèle doivent avoir un titre.",
	'peer_assessment:model:question_delete:success' => "La question a bien été supprimé",
	"peer_assessment:failure:started_already" => "Vous ne pouvez plus modifier le modèle. Le module a déjà été démarré.",
	
		
	//model form lang
	"peer_assessment:model:title" => "Titre du modèle",
	"peer_assessment:model:description" => "Description du modèle",
	"peer_assessment:model:description_before_question" => "Les questions ci-dessous composeront le formulaire dérivé de ce modèle. Chaque question est composée d'un champs de titre et d'un champs de description qui vous permet de donner plus de détails aux utilisateurs et/ou d'expliquer le but de la question.",

	//questions
	"peer_assessment:question" => "Question",
	"peer_assessment:questiondetails" => "Détails de la question ",
	"peer_assessment:add_question" => "Ajouter une question",
	"peer_assessment:delete_question" => "Supprimer cette question",
	
	//form (subgroups)
	'peer_assessment:error:no_form' => "Le modèle n'a pas encore été créé par l'administrateur du groupe parent. Merci de réessayer ultérieurement.",
	'peer_assessment:error:form_model_empty' => "Erreur : le modèle ne contient aucune question. Merci de prévenir l'administrateur du groupe parent.",
	
	'peer_assessment:form:edit' => "Modifier le formulaire d'évaluation par les pairs",
	'peer_assessment:form:view' => "Voir le formulaire d'évaluation par les pairs",
	'peer_assessment:unknown_form' => "Le formulaire d'évaluation par les pairs n'existe pas, ou vous n'avez pas les droits suffisants pour y accèder.",
	'peer_assessment:form:save:success' => "Votre formulaire à bien été enregistré.",
	'peer_assessment:form:sent:success' => "Votre formulaire à bien été envoyé. Vous ne pouvez plus le modifier.",
	'peer_assessment:from:save:error_cannot_edit' => "Vous n'avez pas les droits nécessaire pour modifier ce formulaire. Nous n'avons pas pu l'enregistrer.",
	
	'peer_assessment:form:set_url' => "Lien vers le cadre que vous devez évaluer :",
	
	'peer_assessment:form:send:save_mesage' => "Merci de sauver d'abord votre formulaire avant de l'envoyer, pour être sûr que vos derniers changements ont bien été enregistrés. Une fois le formulaire envoyé, vous ne pourrez plus le modifier.",
	'peer_assessment:form:send_button' => "Envoyer le formulaire",
	'peer_assessment:form:sent:success'=> "Votre formulaire a été envoyé. Vous ne pouvez plus le modifier.",
	"peer_assessment:failure:sent_already" => "Votre formulaire d'évaluation par les pairs a déjà été envoyé. Vous ne pouvez plus le modifier.",
	
	//sets
	'peer_assessment:set:title' => "Cadre d'évaluation par les pairs",
	'peer_assessment:au_sets:noedit' => "Modifier le Cadre d'Evaluation par les Pairs",
	'peer_assessment:au_sets:noedit:info' => "Aucune modification n'est possible pour les paramètres des cadres utilisés au sein du module d'évaluation par les pairs.",
	"acl_view_set_in_group" => "Accès évaluation par les pairs",
	
	//message 
	'peer_assessment:form:message:subject' => "Evaluation par les pairs : Formulaire envoyé par %s",
	'peer_assessment:form:message:body' => " Les membres du groupe %s ont finit de répondre au formulaire d'évaluation par les pairs.
	
Vous pouvez voir leur travail ici : 
%s
	
Vous pouvez voir la liste des paires de l'évaluation par les paires, dans le groupe parent dans le menu 'Gestion du module d\'évaluation par les pairs'.
Vous pouvez aussi y acceder en suivant ce lien : 
%s
	",

);

add_translation('fr', $french);
