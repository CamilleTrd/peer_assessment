<?php
/**
 * @package peer_assessment
 */

$english = array(

	"item:object:answer_peer_assessment"=>"Answers (peer assessment)",
	"item:object:form_peer_assessment"=>"Forms (peer assessment)",
	"item:object:model_peer_assessment"=>"Models (peer assessment)",
	"item:object:question_peer_assessment"=>"Questions (peer assessment)",


	"peer_assessment:peer_assessment" => "Peer Assessment",
	'peer_assessment:enable_peer_assessment' => "Enable peer assessement module?",
	'peer_assessment:parent' => "Parent",
	'peer_assessment:parent:title' => "Peer assessment parent page",
	'peer_assessment:model' => "Model",
	'peer_assessment:form' => "Form",
	
	"peer_assessment:failure:permissiondenied" => "Sorry. You don't have the necessary permission to access this page.",
	
	// Group module
	'peer_assessment:title' => "Peer Assessment",
	'peer_assessment:group_module:none' => "No peer assessment form found yet.",
	
	"peer_assessment:peer_assessment_menu:group_parent" => "Peer assessment management",
	"peer_assessment:peer_assessment_menu:group" => "Peer assessment form",
	
	//parent page
	"peer_assessment:parent:model_desc" => "Create or edit the form model",
	'peer_assessment:parent:model_button' => "Edit/Create",
	'peer_assessment:parent:model_button_view' => "View model",
	"peer_assessment:parent:start_desc" => "Start the peer assessment module. By clicking on the start button, the module will get the list of all the sub-groups of this group and will associate them randomly. It will then create a form according to your model and an evaluation set in each sub-group. You need at least two sub-groups to start the module.",
	"peer_assessment:parent:start_desc_subtext" => "Please make sure that all the sub-groups have been created, and that you are the owner of all of them. If a new sub-group is created after the start it won't be taken into account for the peer assessment. Moreover, if a member joins after the evaluation has started, he wouldn't be able to access some necessary content.",
	'peer_assessment:parent:start_button' => "Start",
	'peer_assessment:parent:list_view_button' => "View pairs",
	'peer_assessment:parent:set_access_info' => "By clicking on the 'Fix sets access' buton, you will fix for each evaluation set in each sub-group, the read access rights. The sets will be available (read only) for all members of both assessor and assessee groups.",
	'peer_assessment:parent:set_access_button' => "Fix sets access",
	'peer_assessment:parent:set_access:success' => "The sets access right have been fixed.",
	'peer_assessment:parent:set_access:fail_cantedit' => "You don't have the necessary permissions to edit the sub-groups sets.",

	
	//Start - launch action
	'peer_assessment:start:failure:no_model' => "Sorry, you can't start the peer assessment module if you haven't define a model. Please create a model and try again.",
	'peer_assessment:start:success' => "The peer assessment module has been started successfully.",
	'peer_assessment:start:failure:already_started' => "The peer assessment module has already been started",
	'peer_assessment:start:error:one_subgrp' => "Sorry, you need at least two sub-groups to be able to start the module.",
	
	//list of subgroups
	'peer_assessment:table:list' => "List of evaluation pairs",
	'peer_assessment:table:group_evaluator' => "Group Assessor",
	'peer_assessment:table:evaluator_of' => "evaluates >",
	'peer_assessment:table:group_evaluee' => "Group Evaluated",
	
	//model (parent group)
	'peer_assessment:model:edit' => "Manage peer assessment form model",
	'peer_assessment:model:view' => "View peer assessment form model",
	'peer_assessment:unknown_model' => "The peer assessment form model requested doesn't exist are you don't have the necessary permissions to edit it.",
	'peer_assessment:model:success_save' => "The model has been successfully saved.",
	'peer_assessment:model:blank_title' => "The model must have a title.",
	'peer_assessment:model:blank_title_question' => "The model's questions must have a title.",
	'peer_assessment:model:question_delete:success' => "The question has been successfully deleted.",
	"peer_assessment:failure:started_already" => "You cannot edit the model anymore. The module has been started already.",
	
		
	//model form lang
	"peer_assessment:model:title" => "Model title",
	"peer_assessment:model:description" => "Model description",
	"peer_assessment:model:description_before_question" => "The questions below are the ones that will compose the forms derived from this model. Each question is made of a main question field and of a description field wich allow you to give more details to the users and/or to explain the goal of the question.",

	//questions
	"peer_assessment:question" => "Question",
	"peer_assessment:questiondetails" => "Question details",
	"peer_assessment:add_question" => "Add a question",
	"peer_assessment:delete_question" => "Delete this question",
	
	//form (subgroups)
	'peer_assessment:error:no_form' => "The form model hasn't been created yet by the admin of the parent group. Please check again later.",
	'peer_assessment:error:form_model_empty' => "Error : The form model contains no questions. Please report this error to the owner of the parent group.",
	
	'peer_assessment:form:edit' => "Edit peer assessment form",
	'peer_assessment:form:view' => "View peer assessment form",
	'peer_assessment:unknown_form' => "The peer assessment form requested doesn't exist are you don't have the necessary permisiions to edit it.",
	'peer_assessment:form:save:success' => "Your peer assessment form has been saved successfully.",
	'peer_assessment:from:save:error_cannot_edit' => "You don't have the necessary permissions to edit this form. We couldn't save it.",
	'peer_assessment:form:sent:success' => "Your peer assessment form has been sent successfully. You cannot edit it anymore.",
	
	'peer_assessment:form:set_url' => "Link to the set you need to evaluate :",
	
	'peer_assessment:form:send:save_mesage' => "Please make sure to first click on the save button before sending the form to make sure your latest changes have been taken into account. Once the form is sent you won't be able to edit it anymore.",
	'peer_assessment:form:send_button' => "Send form",
	'peer_assessment:form:sent:success'=> "Your form has been sent. You can not edit it anymore.",
	"peer_assessment:failure:sent_already" => "Your peer assessment form has been sent already. You cannot edit it anymore.",
	
	//sets
	'peer_assessment:set:title' => "Peer assessment evaluation set",
	'peer_assessment:au_sets:noedit' => "Edit Peer Assessment Set",
	'peer_assessment:au_sets:noedit:info' => "No setting edit is allowed for sets used in the peer assessment module.",
	"acl_view_set_in_group" => "Access peer assessment",
	
	//message 
	'peer_assessment:form:message:subject' => "Peer Assessment : Form sent by %s",
	'peer_assessment:form:message:body' => " The members of the group %s have finished to fill the form for the peer assessmment work.
	
You can view their work here : 
%s
	
You can see the list of pairs for the peer assessment work, in the group parent under the 'peer assessment managment' menu.
You can also follow this link : 
%s
	",

);

add_translation('en', $english);
