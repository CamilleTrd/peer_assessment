<?php
/**
*
* View model form in parent group
* 
* @package peer_assessment
*
**/

// access check for closed groups
group_gatekeeper();

$loggedinuser = elgg_get_logged_in_user_guid();
$group_guid = get_input('group_guid');
$container = get_entity($group_guid);

$title = elgg_echo('peer_assessment:model:view');
elgg_push_breadcrumb(elgg_echo('peer_assessment:parent'), "peer_assessment/group/{$group_guid}/parent");
elgg_push_breadcrumb($title);


$isgrpadmin = is_group_admin($container, $loggedinuser);

if($isgrpadmin){

	$model = get_peer_assessment_entity_by_container($group_guid, 'model_peer_assessment');
		
	$content = elgg_view_entity($model, array('full_view' => true));
	
}else{
	$content = elgg_echo("peer_assessment:failure:permissiondenied");
}

$body = elgg_view_layout('content', array(
	'filter' => '',
	'content' => $content,
	'title' => $title,
));

echo elgg_view_page($title, $body);