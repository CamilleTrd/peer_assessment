<?php
/**
*
* Edit form in subgroups
* @package peer_assessment
*
**/

// access check for closed groups
group_gatekeeper();

$group_guid = get_input('group_guid');
$form = get_peer_assessment_entity_by_container($group_guid, 'form_peer_assessment');

$title = elgg_echo('peer_assessment:form:edit');
elgg_push_breadcrumb(elgg_echo('peer_assessment:form'), "form");
elgg_push_breadcrumb($title);

if (!$form->sent){
	$body_vars['form_guid'] = $form->guid;
	$content = elgg_view_form('peer_assessment/save', array(), $body_vars);
	$content .= elgg_view_comments($form);
}else{
	$content = elgg_echo("peer_assessment:failure:sent_already");
}
$body = elgg_view_layout('content', array(
	'filter' => '',
	'content' => $content,
	'title' => $title,
));

echo elgg_view_page($title, $body);