<?php
/**
*
* View list of subgroups paired in parent group
* 
* @package peer_assessment
*
**/

// access check for closed groups
group_gatekeeper();

$loggedinuser = elgg_get_logged_in_user_guid();
$group_guid = get_input('group_guid');
$container = get_entity($group_guid);

$title = elgg_echo('peer_assessment:table:list');
elgg_push_breadcrumb(elgg_echo('peer_assessment:parent'), "peer_assessment/group/{$group_guid}/parent");
elgg_push_breadcrumb($title);


$isgrpadmin = is_group_admin($container, $loggedinuser);

if($isgrpadmin){
	//retreive all subgroups
	$array_subgroups = get_group_parent($group_guid, true);
	
	$model = get_peer_assessment_entity_by_container($group_guid, 'model_peer_assessment');
	
	$grp_eval_final = array();
	//get evaluee of each subgrp
	foreach($array_subgroups as $subgrp){		
		$grp_eval = get_subgroup_evaluator($subgrp->guid, false, $model->guid);
		$grp_eval_final[$subgrp->guid] = $grp_eval;
	}
	
	//set up table header
	$content .= "<div><br><table><tr>";
	$content .= "<th width=\"30%\"><b>".elgg_echo('peer_assessment:table:group_evaluator')."</b></th>";
    $content .= "<th width=\"20%\"><b>".elgg_echo('peer_assessment:table:evaluator_of')."</b></th>";
    $content .= "<th width=\"30%\"><b>".elgg_echo('peer_assessment:table:group_evaluee')."</b></th>";
    $content .= "<tr><td colspan=3><hr></td></tr>";
    
     //build table content ligne by ligne
    foreach($grp_eval_final as $key => $value){
    	//grp 1 is evaluator
    	$grp1 = get_entity($key);
    	//grp 2 is assessee
    	$grp2 = $value;
		    	
        $content .= "<tr>";
        $content .= "<td><a href=\"{$grp1->getURL()}\">{$grp1->name}</a></td>";
        $content .= "<td>".elgg_echo('peer_assessment:table:evaluator_of')."</td>";
        $content .= "<td><a href=\"{$grp2->getURL()}\">{$grp2->name}</a></td>";
        $content .= "</tr>";

    }

	
    $content .= "</table></div>";


}else{
	$content = elgg_echo("peer_assessment:failure:permissiondenied");
}

$body = elgg_view_layout('content', array(
	'filter' => '',
	'content' => $content,
	'title' => $title,
));

echo elgg_view_page($title, $body);