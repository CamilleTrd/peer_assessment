<?php
/**
*
* Welcome page in parent group
* 
* @package peer_assessment
*
**/
// access check for closed groups
group_gatekeeper();

$group_guid = get_input('group_guid');
$container = get_entity($group_guid);

$loggedinuser = elgg_get_logged_in_user_guid();

$title = elgg_echo('peer_assessment:parent:title');
elgg_push_breadcrumb(elgg_echo('peer_assessment:parent'));
elgg_push_breadcrumb($container->name, $container->getURL());

$isgrpadmin = is_group_admin($container, $loggedinuser);

$started = pa_is_started($group_guid);

if($isgrpadmin){
	
	// go to model edit page
	$model_url = $vars['url'] ."peer_assessment/group/{$group_guid}/model";
	// go to model view page
	$model_url_view = $vars['url'] ."peer_assessment/group/{$group_guid}/model_view";
	// go to list view page
	$list_view_url = $vars['url'] ."peer_assessment/group/{$group_guid}/list_subgroups";
	//launch start action
	$start_url = $vars['url'] ."action/peer_assessment/launch?group_guid={$group_guid}" ;
	//launch start action
	$set_access_url = $vars['url'] ."action/peer_assessment/set_access?group_guid={$group_guid}" ;

	$model_button = elgg_view('output/url', array(
           'text' => elgg_echo('peer_assessment:parent:model_button'),
           'href' => $model_url,
           'class' => 'elgg-button elgg-button-action',
           'is_action' => FALSE
  		 ));

	$model_view_button = elgg_view('output/url', array(
           'text' => elgg_echo('peer_assessment:parent:model_button_view'),
           'href' => $model_url_view,
           'class' => 'elgg-button elgg-button-action',
           'is_action' => FALSE
  		 ));
  		 
  	$list_view_button = elgg_view('output/url', array(
           'text' => elgg_echo('peer_assessment:parent:list_view_button'),
           'href' => $list_view_url,
           'class' => 'elgg-button elgg-button-action',
           'is_action' => FALSE
  		 ));
	
	$start_button = elgg_view('output/url', array(
           'text' => elgg_echo('peer_assessment:parent:start_button'),
           'href' => $start_url,
           'class' => 'elgg-button elgg-button-action',
           'is_action' => TRUE
  		 ));

	$set_access_button = elgg_view('output/url', array(
           'text' => elgg_echo('peer_assessment:parent:set_access_button'),
           'href' => $set_access_url,
           'class' => 'elgg-button elgg-button-action',
           'is_action' => TRUE
  		 ));



	$content .= "<div class='parent_section'> <p class='parent_desc'>".elgg_echo("peer_assessment:parent:model_desc")."</p>";
	if (!$started){
		$content .= "<span> ".$model_button."</span>";
	}
	$content .= "<span> ".$model_view_button."</span>";
	$content .= "</div>";
	$content .= "<div class='parent_section'><p class='parent_desc'>".elgg_echo("peer_assessment:parent:start_desc")."</p>";
	$content .= "<p class='parent_desc_subtext'>".elgg_echo("peer_assessment:parent:start_desc_subtext")."</p>";
	if (!$started){
		$content .= "<span> ".$start_button."</span></div>";
	}else{
		$content .= "<span> ".$list_view_button."</span></div>";
		$content .= "<div class='parent_section'><p class='parent_desc'>".elgg_echo('peer_assessment:parent:set_access_info')."</p>";
		$content .= "<span> ".$set_access_button."</span></div>";
	}
	
}else{
	$content = elgg_echo("peer_assessment:failure:permissiondenied");
}

$body = elgg_view_layout('content', array(
	'filter' => '',
	'content' => $content,
	'title' => $title,
));

echo elgg_view_page($title, $body);