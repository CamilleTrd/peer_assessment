<?php
/**
*
* Edit model form in parent group
* 
* @package peer_assessment
*
**/

// access check for closed groups
group_gatekeeper();

$loggedinuser = elgg_get_logged_in_user_guid();
$group_guid = get_input('group_guid');
$container = get_entity($group_guid);

$started = pa_is_started($group_guid);

$title = elgg_echo('peer_assessment:model:edit');
elgg_push_breadcrumb(elgg_echo('peer_assessment:parent'), "peer_assessment/group/{$group_guid}/parent");
elgg_push_breadcrumb($title);

if(!$started){

	$isgrpadmin = is_group_admin($container, $loggedinuser);
	
	if($isgrpadmin){
	
		$model = get_peer_assessment_entity_by_container($group_guid, 'model_peer_assessment');
		
		$body_vars = peer_assessment_prepare_form_vars_model($model);
		
		$content = elgg_view_form('peer_assessment/save_model', array(), $body_vars);
	
	}else{
		$content = elgg_echo("peer_assessment:failure:permissiondenied");
	}
}else{
	$content = elgg_echo("peer_assessment:failure:started_already");
}

$body = elgg_view_layout('content', array(
	'filter' => '',
	'content' => $content,
	'title' => $title,
));

echo elgg_view_page($title, $body);