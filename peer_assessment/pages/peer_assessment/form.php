<?php
/**
*
* View form in sub-group after send
* 
* @package peer_assessment
*
**/

// access check for closed groups
group_gatekeeper();

$group_guid = get_input('group_guid');

$title = elgg_echo('peer_assessment:form:view');
elgg_push_breadcrumb(elgg_echo('peer_assessment:form'));
elgg_push_breadcrumb($title);

$form = get_peer_assessment_entity_by_container($group_guid, 'form_peer_assessment');
	
$content = elgg_view_entity($form, array('full_view' => true));
$content .= elgg_view_comments($form);
	
$body = elgg_view_layout('content', array(
	'filter' => '',
	'content' => $content,
	'title' => $title,
));

echo elgg_view_page($title, $body);