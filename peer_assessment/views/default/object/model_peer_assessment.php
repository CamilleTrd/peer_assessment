<?php
/**
 * Peer_assessment model view object
 *
 * @package peer_assessment
 */
 

$full = elgg_extract('full_view', $vars, FALSE);
$model = elgg_extract('entity', $vars, FALSE);

if (!$model) {
	return true;
}

$owner = $model->getOwnerEntity();
$container = $model->getContainerEntity();

$owner_link = elgg_view('output/url', array(
	'href' => "groups/profile/$model->owner_guid/$owner->name",
	'text' => $owner->name,
	'is_trusted' => true,
));

if (elgg_get_page_owner_guid() == $model->container_guid) {
	$owner_icon = elgg_view_entity_icon($owner, 'small');
} else {
	$owner_icon = elgg_view_entity_icon($container, 'small');
}

$owner_link = elgg_view('output/url', array(
	'href' => $container->getURL(),
	'text' => $owner->name,
));

$date = elgg_view_friendly_time($model->time_created);
$author_text = elgg_echo('byline', array($owner_link));

$subtitle = "$author_text $date";

// do not show the metadata and controls in widget view
if (!elgg_in_context('widgets')) {
	$metadata = elgg_view_menu('entity', array(
		'entity' => $vars['entity'],
		'handler' => 'peer_assessment',
		'sort_by' => 'priority',
		'class' => 'elgg-menu-hz',
	));
}

if ($full) {
	$body = "<div class='model-header'>".elgg_view_title($model->title);
	$body .= elgg_view('output/longtext', array('value' => $model->description))."</div>";
	
	// add questions 
	$questions_array = get_questions_by_model_guid($model->guid);
	
	$body .= "<div id='model_questions'> ";
	
	foreach($questions_array as $question){
		$body .= "<div id='question'> ";
		$body .= "<span class='question_title_view'>". $question->title ."</span>";
		$body .= "<div class='question_details_view'>". elgg_view('output/longtext', array('value' => $question->description))."</div>";
		$body .= "</div>";
	}
	
	$body .= "</div>";
	
	$params = array(
		'entity' => $model,
		'metadata' => $metadata,
		'subtitle' => $subtitle,
	);
	
	$params = $params + $vars;
	$summary = elgg_view('object/elements/summary', $params);

	echo elgg_view('object/elements/full', array(
		'entity' => $model,
		'title' => false,
		//'icon' => $model_icon,
		//'summary' => $summary,
		'body' => $body,
	));

}
//no brief view in group parent for model
/* else {
	// brief view

	$excerpt = elgg_get_excerpt($model->description);

	$params = array(
		'entity' => $model,
		'metadata' => $metadata,
		'subtitle' => $subtitle,
		'content' => $excerpt,
	);
	$params = $params + $vars;
	$list_body = elgg_view('object/elements/summary', $params);

	echo elgg_view_image_block($model_icon, $list_body);
}*/
