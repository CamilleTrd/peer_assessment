<?php
/**
 * Peer_assessment form view
 *
 * @package peer_assessment
 */

$full = elgg_extract('full_view', $vars, FALSE);
$form = elgg_extract('entity', $vars, FALSE);

if (!$form) {
	return true;
}

//the owner and the container of the form is the sub-group
$owner = $form->getOwnerEntity();
$container = $form->getContainerEntity();

$owner_link = elgg_view('output/url', array(
	'href' => "groups/profile/$form->owner_guid/$owner->name",
	'text' => $owner->name,
	'is_trusted' => true,
));

if (elgg_get_page_owner_guid() == $form->container_guid) {
	$owner_icon = elgg_view_entity_icon($owner, 'small');
} else {
	$owner_icon = elgg_view_entity_icon($container, 'small');
}

$date = elgg_view_friendly_time($form->time_created);
$author_text = elgg_echo('byline', array($owner_link));

$comments_count = $form->countComments();
//only display if there are commments
if ($comments_count != 0 && !$revision) {
	$text = elgg_echo("comments") . " ($comments_count)";
	$comments_link = elgg_view('output/url', array(
		'href' => $form->getURL() . '#form-comments',
		'text' => $text,
		'is_trusted' => true,
	));
} else {
	$comments_link = '';
}

$subtitle = "$author_text $date $comments_link";

// do not show the metadata and controls in widget view
if (!elgg_in_context('widgets')) {
	$metadata = elgg_view_menu('entity', array(
		'entity' => $vars['entity'],
		'handler' => 'peer_assessment',
		'sort_by' => 'priority',
		'class' => 'elgg-menu-hz',
	));
}

if ($full) {
	
	$body = "<div class='model-header'>".elgg_view_title($form->title);
	$body .= elgg_view('output/longtext', array('value' => $form->description))."</div>";
	
	// add questions 
	$questions_array = get_questions_by_model_guid($form->model);
	
	$body .= "<div id='model_questions'> ";
	
	foreach($questions_array as $question){
		$body .= "<div id='question'> ";
		$body .= "<span class='question_title_view'>". $question->title ."</span>";
		$body .= "<div class='question_details_view'>". elgg_view('output/longtext', array('value' => $question->description))."</div>";
		$body .= "</div>";
		
		$answer = get_answer_by_question_and_form($question->guid, $form->guid);
		
		$body.= "<div class='answer_view'>".$answer->description."</div>";
	}
	
	$body .= "</div>";
	
	$params = array(
		'entity' => $form,
		'metadata' => $metadata,
		'subtitle' => $subtitle,
	);
	
	$params = $params + $vars;
	$summary = elgg_view('object/elements/summary', $params);

	echo elgg_view('object/elements/full', array(
		'entity' => $form,
		'title' => false,
		//'icon' => $form_icon,
		//'summary' => $summary,
		'body' => $body,
	));

} 

// no brief view needed here

/*else {
	// brief view

	$excerpt = elgg_get_excerpt($form->description);

	$params = array(
		'entity' => $form,
		'metadata' => $metadata,
		'subtitle' => $subtitle,
		'content' => $excerpt,
	);
	$params = $params + $vars;
	$list_body = elgg_view('object/elements/summary', $params);

	echo elgg_view_image_block($form_icon, $list_body);
}*/
