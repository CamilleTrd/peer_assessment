<?php
/**
*
* Edit form in sub-groups
*
* @package peer_assessment
*
**/

$form_guid = $vars['form_guid'];

$form = get_entity($form_guid);

if(!$form){
	echo elgg_echo('peer_assessment:error:no_form');
	
}else{
	$array_questions = array();
	$array_questions = get_questions_by_model_guid($form->model);

	$action = "peer_assessment/save";
	
	//launch start action
	$send_url = $vars['url'] ."action/peer_assessment/send?form_guid={$form_guid}" ;
	$send_button = elgg_view('output/url', array(
	           'text' => elgg_echo('peer_assessment:form:send_button'),
	           'href' => $send_url,
	           'class' => 'elgg-button elgg-button-action',
	           'is_action' => TRUE
	  		 ));
	
	
	?>
	<div class="contentWrapper">
	
		<!-- display the input form -->
		<form action="<?php echo $vars['url']; ?>action/<?php echo $action; ?>" method="post">
			
	 		<?php
	 		// if no questions -> model is empty erreur !!!
			if (empty($array_questions)){
				//ERROR
				echo elgg_echo("peer_assessment:error:form_model_empty");
			}else{
			//display form title and description	
			?>
				<div class='model-header'> 
					<?php
					echo elgg_view_title($form->title);
					echo elgg_view('output/longtext', array('value' => $form->description));
					?>
				</div>		    			
			<?php
			
			//print questions	
				 foreach ($array_questions as $question) {
				 	$answer = get_answer_by_question_and_form($question->guid, $form_guid);
			?>
			
					<div id="question-answer">
						<!--echo question title-->
						<label> <?php echo $question->title; ?> </label>
						<!--echo question descritpion-->
						<div class= "question_details_view"><?php echo $question->description; ?></div>
						
						<!-- question answer input -->
						<?php
							echo elgg_view('input/hidden', array('name' => 'answers_guids[]', 'value' => $answer->guid));
				
							echo elgg_view("input/longtext", array(
								"name" => "answers_texts[]",
								"value" => $answer->description,
							));
						?>
						
					</div>
			<?php
				 }//End foreach
			} //end else
			?>
			
			<!-- required hidden info and submit button -->
			<p style='color:red;'><?php echo elgg_echo('peer_assessment:form:send:save_mesage');	?></p>
			<p>
				<?php 		
					echo elgg_view('input/submit', array('value' => elgg_echo("save"))); 
					
					echo "<span style='float:right;'>".$send_button."</span>";
				?>
			</p>
			<?php
			echo elgg_view('input/securitytoken');
			echo elgg_view('input/hidden', array('name' => 'model_guid', 'value' => $form->model));
			echo elgg_view('input/hidden', array('name' => 'group_guid', 'value' => $form->container_guid));
			echo elgg_view('input/hidden', array('name' => 'form_guid', 'value' => $form->guid));
			
			?>
		</form>
	</div>

<?php
}//end else
?>