<?php
/**
*
* Edit model form in parent group
* @package peer_assessment
*
**/

// Set title, form destination

//retreive existing model if already exist

$group_guid = get_input('group_guid');

//$access_id = elgg_extract('access_id', $vars, ACCESS_DEFAULT);

$container_guid = elgg_extract('container_guid', $vars, $group_guid);
$model_guid = elgg_extract('guid', $vars, null);

$question_guids = elgg_extract('question-guids', array());
$question_titles = elgg_extract('question-titles', array());
$question_details = elgg_extract('question-details', array());


if ($model_guid){
	
	$model = get_entity($model_guid);
 	// query DB only if model exist.
	$array_questions = get_questions_by_model_guid($model_guid);
	
	$model_title = $model->title;
	$model_description = $model->description;
	
}else{
	
	$model_title = elgg_extract('title', $vars, '');
	$model_description = elgg_extract('description', $vars, '');

}

$action = "peer_assessment/save_model";

$count = 2;
?>

	<!-- display the input form -->
	<form action="<?php echo $vars['url']; ?>action/<?php echo $action; ?>" method="post">
		<div class="model-header">
		<!-- model title -->
		<p>
	      <label><?php echo elgg_echo('peer_assessment:model:title'); ?></label>
	        <?php echo elgg_view("input/text", array(
	        				"name" => "model-title", 
	        				"value" => $model_title
	        			)); 
	       	?>
	      
	     <p/>
	     <!-- model description -->
	       <label> <?php echo elgg_echo('peer_assessment:model:description'); ?></label>
	        <?php echo elgg_view("input/longtext", array(
	        				"name" => "model-description", 
	        				"value" => $model_description
	        			)); 
	       	?>

	    </div>
	     <p class ="model-question-desc"><?php echo elgg_echo("peer_assessment:model:description_before_question");?></p>
		
		
		
		<div id="questions-array">
			<a id="add-question" href="#"><?php echo elgg_echo("peer_assessment:add_question") ?></a>
			
			
			<?php
	 		// if no questions -> model is empty
			if (empty($array_questions)){
			?>
			<div id="question1">
			
				<?php echo elgg_view('input/hidden', array('name' => 'question-guids[]', 'value' => 0)); ?>
				<!-- question title -->
				<p>
					<label><?php echo elgg_echo("peer_assessment:question")." - 1"; ?></label>
					<?php echo elgg_view("input/text", array(
									"name" => "question-titles[]"
								));
					?>
				
				</p>
				<!-- question description input -->
				<label><?php echo elgg_echo("peer_assessment:questiondetails")." - 1"; ?></label>
				<?php echo elgg_view("input/longtext", array(
								"name" => "question-details[]"
							));
				?>
		
			</div>
		
		
		
			<?php
			//if model has questions-> edit
			}else{
				// start at 1 and not at 2 for question number
				$count--;
				 foreach ($array_questions as $question) {
				 	
			?>
		
			<div id="question<?=$count?>">
				<?php echo elgg_view('input/hidden', array('name' => 'question-guids[]', 'value' => $question->guid)); ?>
				<p>
					<!-- question title -->
					<label><?php echo elgg_echo("peer_assessment:question")." - ".$count; ?></label>
					<?php echo elgg_view("input/text", array(
									"name" => "question-titles[]",
									"value" => $question->title,
								));
					?>
				</p>
		
				<!-- question description input -->
				<label><?php echo elgg_echo("peer_assessment:questiondetails")." - ".$count; ?></label>
				<?php
					echo elgg_view("input/longtext", array(
						"name" => "question-details[]",
						"value" => $question->description,
					));
					
					if($count>1){
					//create button for delete saved question
					$delete_question_url = $vars['url'] ."action/peer_assessment/delete_question?question_guid={$question->guid}" ;
					$delete_question_button = elgg_view('output/url', array(
			           'text' => elgg_echo('peer_assessment:delete_question'),
			           'href' => $delete_question_url,
			           'class' => "delete-question",
			           'is_action' => TRUE
 		 			));
 		 			
 		 			echo $delete_question_button;
					}
				?>
			</div>
		
		<?php
			$count ++;
			} //end foreach
		} //end else
		?>
		</div>
		<!-- required hidden info and submit button -->
		<div class="elgg-foot">
			<p style='padding-top = 20px;'>
				<?php echo elgg_view('input/submit', array('value' => elgg_echo("save"))); ?>
			</p>
			<?php
			echo elgg_view('input/securitytoken');
	
			echo elgg_view('input/hidden', array('name' => 'container_guid', 'value' => $container_guid));
			echo elgg_view('input/hidden', array('name' => 'model_guid', 'value' => $model_guid));
			
			?>
		</div>
	</form>
	
	
<!-- to dynamically add and delete question when creating form -->
<script type="text/javascript">
  var counter= '<?php echo $count; ?>';
  var textAreas = new Array();
  
  $('document').ready(function(){
    $("#add-question").click(function(){
     	var newdiv = document.createElement('div');
     	newdiv.setAttribute("id", "question"+counter);

     	$.getJSON( elgg.config.wwwroot + 'mod/peer_assessment/lib/dynamic_form.php',
			  {counter : counter},
			  function(data) {
			  		newdiv.innerHTML = data.html;
			  		manageTextareaId(data.id, counter);
			  	});
			  	
		     	
       // document.getElementById('questions-array').appendChild(newdiv);
        $("#questions-array").append(newdiv);
           
      counter++;
      return false;
    });
  });
  
  function manageTextareaId(id, counter){ 
	textAreas[counter] = id;
	tinyMCE.execCommand('mceAddControl', false, textAreas[counter]);
  }
  
  function del_options(id){
  	if(id>1){
  		
   		$("#question"+id).remove();
  	}
  }
  
 </script>