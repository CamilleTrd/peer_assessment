<?php
/**
* CSS 
*
* @package peer_assessment
**/
?>

/* parent page*/

.parent_section{
	padding-bottom : 50px;
}


.parent_desc{
	text-align:justify;
}


.parent_desc_subtext{
	font-style:italic;
	background: transparent url(<?php echo elgg_get_site_url();?>mod/peer_assessment/graphics/exclamation.png) no-repeat ;
	padding-left: 50px;
	text-align:justify;
	height : 50px;
	vertical-align:middle;
	padding-bottom: 15px;
}


/* model page */

.model-header{
	background : #f7f6f2;	
	padding : 10px;
}

#add-question{
	float: right;
	background: transparent url(<?php echo elgg_get_site_url();?>mod/peer_assessment/graphics/add.png) no-repeat ;
	padding-left: 20px;
	padding-bottom : 10px;
}

.delete-question{
	float: right;
	background: transparent url(<?php echo elgg_get_site_url();?>mod/peer_assessment/graphics/delete.png) no-repeat ;
	padding-left: 20px;
	padding-bottom : 10px;

}

.model-question-desc{
	padding : 10px;
	font-style:italic;
	text-align:justify;
}

div[id*='question'] {
	padding-bottom : 15px;
	padding-top : 10px;
}

/* view objects */

#model_questions{
	padding : 10px;
}

#question{
	margin-top : 10px;
}

.question_title_view{
	text-align:justify;
	font-weight:bold;
	margin-bottom : 10px;
}

.question_details_view {
	background: #EAEAEA;
	font-style:italic;
	text-align:justify;
}

.answer_view {
	text-align:justify;
}
