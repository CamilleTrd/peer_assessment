<?php
/**
 * Group peer assessment module
 * @package peer_assessment
 */

$group = elgg_get_page_owner_entity();

$group_parent = get_group_parent($group->guid);

// if no group parent exist and P-A not enable in this group then quit
if (!$group_parent && $group->peer_assessment_enable == "no"){
	return true;
// if group parent exist and P-A not enable in group parent then quit
}else ($group_parent->peer_assessment_enable == "no") {
	return true;
}

//retrieve form to fill
elgg_push_context('widgets');
$options = array(
	'type' => 'object',
	'subtype' => 'form_peer_assessment',
	'container_guid' => $group->guid,
	'limit' => 2,
	'full_view' => false,
	'pagination' => false,
);
$content = elgg_list_entities($options);
elgg_pop_context();


if (!$content) {
	$content = '<p>' . elgg_echo('peer_assessment:group_module:none') . '</p>';
}

echo elgg_view('groups/profile/module', array(
	'title' => elgg_echo('peer_assessment:title'),
	'content' => $content,
));
