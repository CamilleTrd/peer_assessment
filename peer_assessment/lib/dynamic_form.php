<?php
// Load Elgg engine
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/engine/start.php');

/**
*
* Call by jquery to enable dynamic form in model creation 
*
* @peer_assessment package
*
**/


$counter = get_input('counter', 'false');
$textarea = elgg_view("input/longtext", array("name" =>"question-details[]"));
$textarea_html = trim(preg_replace('/\s+/', ' ', $textarea));

preg_match('/id="([^"]*)"/', $textarea, $match);
$textarea_id = $match[1];


$html = elgg_view('input/hidden', array('name' => 'question-guids[]', 'value' => 0)).'<p><label>'. elgg_echo("peer_assessment:question") .' -  '. $counter.'</label>'. elgg_view("input/text", array("name" => "question-titles[]")) .'</p><label>'.  elgg_echo("peer_assessment:questiondetails") .' - '. $counter .'</label>'. $textarea_html .'<a class="delete-question" href="javascript:del_options('.$counter.');" >'. elgg_echo("peer_assessment:delete_question") .'</a>';
 
echo json_encode(array("html" => $html, "id" => $textarea_id)); 