<?php
/**
 * peer assessment helper functions
 *
 * @package peer_assessment
 */


/*
* retreive all questions with model_guid (containers of question object)
*/
function get_questions_by_model_guid($model_guid){
	
	//inverse relationship means guid is guid 2 in BDD
	//order by ID to retreive question chronologic order.
	
	$options =	array( 
	 	 'type' => 'object',
		 'subtype' => 'question_peer_assessment',
    	 'container_guid' => $model_guid,
    	 'order_by' => 'guid asc',
    	 'limit' => 0

   	);
	     		
     $array_questions = elgg_get_entities($options);
     
     return $array_questions ;
}

/*
* retreive all form guids by metadata with model_guid

function get_form_by_model_guid($model_guid){
	
	//inverse relationship means guid is guid 2 in BDD
	
	 $options =	array( 
		 'metadata_names' => 'model',
    	 'metadata_values' => $model_guid,
   	);
	     		
     $array_forms = elgg_get_entities_from_metadata($options);
     
     return $array_forms ;
}
*/

/*
* retrieve the answer for a given question and for a given form 
*/
function get_answer_by_question_and_form($question_guid, $form_guid){
		
	 $options =	array( 
		 'metadata_names' => 'question',
    	 'metadata_values' => $question_guid,
    	 'type' => 'object',
		 'subtype' => 'answer_peer_assessment',
    	 'container_guid' => $form_guid,
   	);
      
   	//add metadata search container_guid = $form_guid??? instead of foreach.
         
     $answers_array = elgg_get_entities_from_metadata($options);
         
      // find answer in array that is contained in form_guid
      
      foreach ($answers_array as $ans){
      	
  	      	if ($ans->container_guid == $form_guid){
 	      		
      			$answer = $ans;
      			//there is only one answer object for a given question and a given form
      			break;
      		}
      }
      

          
      return $answer ;
}

/*
* create evaluation set in sub_group
*/
function create_evaluation_set($grp_assessee_guid, $group_assessor_guid){
	
	$group = get_entity($grp_assessee_guid);
	$group_assessor = get_entity($group_assessor_guid);
	
	//check if sets are enable in sub-groups, if not enable them
	if($group->sets_enable == 'no'){
		$group->setMetaData('sets_enable', 'yes');
	}
	
	//create access_collection for the set
	$name = elgg_echo("acl_view_set_in_group")." : ".$grp_assessee_guid;
	$owner_guid = $grp_assessee_guid;
	
	$collection_id = create_access_collection($name, $owner_guid);
	
	//get members of the 2 groups
	$group_members_assessee = elgg_get_entities_from_relationship(
		array(
	         'relationship' => 'member',
	         'relationship_guid' => $grp_assessee_guid,
	         'inverse_relationship' => TRUE,
	         'type' => 'user',
	     ));

	$group_members_assessor = elgg_get_entities_from_relationship(
		array(
	         'relationship' => 'member',
	         'relationship_guid' => $group_assessor_guid,
	         'inverse_relationship' => TRUE,
	         'type' => 'user',
	     ));
	//mere array of members 
	$groups_members = array_merge($group_members_assessee, $group_members_assessor);
	
	//add each mamaber in collection access
	foreach($groups_members as $member){
		add_user_to_access_collection($member->guid, $collection_id);
	}
				
	
	$set = new ElggObject();
	$set->subtype = 'au_set';
	$set->title = elgg_echo('peer_assessment:set:title').$group->guid;
	$set->access_id = $collection_id;
	$set->write_access_id = $group->group_acl;
	$set->comments_on = 'On';
	$set->tags = '';
	$set->layout = '[[100],[33,34,33],[100]]';
	
	$set->container_guid = $grp_assessee_guid;
	$set->owner_guid = $grp_assessee_guid;
	
	if($set->save()){
		$set_return = $set;
	}else{
		$set_return = null;
	}
	
	return $set_return;
}

/*
* create form for evaluation set in sub_group
*/
function create_form($grp_eval_guid, $set, $model){
	
	$set_url = $set->getURL();

	//define  $access_id as grp_eval and parent group owner(read only)???
	$grp_eval = get_entity($grp_eval_guid);
	
	$descritpion = $model->description." <br/> ".elgg_echo('peer_assessment:form:set_url')." <a href='".$set_url."'> ".elgg_echo('peer_assessment:set:title')." </a>";
	
	$form = new ElggObject();

	//create form 
	$form->title = $model->title;
	//concat $set_url to $model->descritpion 
	$form->description = $descritpion;
	//form in grp_eval. No owner : owner is grp not a member.
	$form->container_guid = $grp_eval_guid;
	$form->owner_guid = $grp_eval_guid;
	$form->access_id = $grp_eval->group_acl;
	$form->subtype = "form_peer_assessment";
	
	//metadata : save model guid in form object
	$form->model = $model->guid;
	$form->set = $set->guid;
	$form->set_collection_id = $set->access_id;
		
	//save form
	if($form->save()){
		// create answers
		$array_questions = get_questions_by_model_guid($model->guid);
		foreach($array_questions as $question){
			
			$answer = new ElggObject();
			$answer->title = "answer of grp :".$grp_eval->name." , question ".$question->title;
			$answer->description = "";
			//answer contained in form. No owner : owner is grp not a member.
			$answer->container_guid = $form->guid;
			$answer->owner_guid = $grp_eval_guid;
			$answer->access_id = $grp_eval->group_acl;
			$answer->write_access_id = $grp_eval->group_acl;
			$answer->subtype = "answer_peer_assessment";
			
			$answer->question = $question->guid;
			
			$answer->save();
		}
	}
}


/*
* retrieve the parent group of a given subgroup
* if $inverse = true -> retreive subgroups of the group given in parameter
*/
function get_group_parent($group_guid, $inverse = false){
	
	$options = array(
		'relationship' => 'au_subgroup_of',
		'relationship_guid' => $group_guid,
		'inverse_relationship' => $inverse,
		'limit' => 0
	);
			
	$group_parent_array = elgg_get_entities_from_relationship($options);
	if($inverse == false){
		//Search parent so only one group
		return $group_parent_array[0];
	}else{
		return $group_parent_array;
	}
}


/*
* get evaluator group
*/
function get_subgroup_evaluator($group_guid, $inverse = true, $model_guid){
	
	//$group_guid is evaluating the researched other group
	$options = array(
		'relationship' => 'evaluates_pa_'.$model_guid,
		'relationship_guid' => $group_guid,
		'inverse_relationship' => $inverse
	);
			
	$group_evaluator_array = elgg_get_entities_from_relationship($options);
	
	//should return only one group entity
	return $group_evaluator_array[0];
}

/*
* retrieve form or model according to a container (group)
*/
function get_peer_assessment_entity_by_container($container_guid, $subtype){
	
	$options = array(
		'type' => 'object',
		'subtype' => $subtype,
		'container_guid' => $container_guid
		);
	
	$entity = elgg_get_entities($options);

	return $entity[0];
}

/*
* check if peer assessment ha
*/

function pa_is_started($group_guid){
	
	$model = get_peer_assessment_entity_by_container($group_guid, 'model_peer_assessment');
	$started = false;
	
	if(!empty($model) && ($model->started == true)){
		$started = true;
	}
	
	// boolean
	return $started;
}

/*
* Check if loggedin user is group admin or group owner 
*/
function is_group_admin($container, $loggedinuser){
	//check if user is group admin or group owner
		$isgrpadmin = false;
		
		//inverse relationship means guid is guid 2 in BDD
		$opts = array( 
					'relationship' => 'group_admin',
					'relationship_guid' => $container->guid,
					'inverse_relationship' => true
		     	 );
		$grp_admins_ent = elgg_get_entities_from_relationship($opts);
		
		unset($grp_admins);
		foreach($grp_admins_ent as $grpAdmin){
			$grp_admins[] = $grpAdmin->guid;
		}
		
		// is grp admins or group owner
		if($loggedinuser == $container->owner_guid || in_array($loggedinuser,$grp_admins)){
			$isgrpadmin = true;
		}

	return $isgrpadmin;
}

/*
* Sticky form handling for model
*/
function peer_assessment_prepare_form_vars_model($model = null) {
	// input names => defaults
	$values = array(
		'title' => get_input('model-title', ''),
		'description' => get_input('model-description',''),
		'question-guids' => get_input('question-guids', array()),
		'question-titles' => get_input('question-titles', array()),
		'question-details' => get_input('question-details', array()),
		'access_id' => ACCESS_DEFAULT,
		'container_guid' => elgg_get_page_owner_guid(),
		'guid' => null,
		'entity' => $model,
	);
 
	if ($model) {
		foreach (array_keys($values) as $field) {
			if (isset($model->$field)) {
				$values[$field] = $model->$field;
			}
		}
	}
 
	if (elgg_is_sticky_form('model')) {
		$sticky_values = elgg_get_sticky_values('model');
		foreach ($sticky_values as $key => $value) {
			$values[$key] = $value;
		}
	}
 
	elgg_clear_sticky_form('model');
 
	return $values;
}

