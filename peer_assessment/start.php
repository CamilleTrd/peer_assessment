<?php
/**
 *
 * Start file
 * @package peer_assessment
 *
 */

// register a library of helper functions
//require_once dirname(__FILE__) . "/lib/peer_assessment.php";
  
function peer_assessment_init() {
	// register a library of helper functions
	elgg_register_library('elgg:peerassessment', elgg_get_plugins_path() . 'peer_assessment/lib/peer_assessment.php');
		
	elgg_load_library('elgg:peerassessment');
	
	 // replace the existing groups library so we can push some display options
  	elgg_register_library('au_sets', elgg_get_plugins_path() . 'peer_assessment/lib/au_sets.php');
  	elgg_load_library('au_sets');
	
	// add our own css
	elgg_extend_view('css/elgg', 'peer_assessment/css');
	
	// the peer_assessment object is the form filled by each subgroup
	elgg_register_entity_type('object', 'question_peer_assessment');
	elgg_register_entity_type('object', 'answer_peer_assessment');
	elgg_register_entity_type('object', 'model_peer_assessment');
	elgg_register_entity_type('object', 'form_peer_assessment');
	

	// Register a script to handle (usually) a POST request (an action)
	$actions_dir = dirname(__FILE__) . "/actions";
	// save form model in parent group
	elgg_register_action("peer_assessment/save_model", "$actions_dir/save_model.php");
	// delete question in form model in parent group
	elgg_register_action("peer_assessment/delete_question", "$actions_dir/delete_question.php");
	// student send evaluation to parent group
	elgg_register_action("peer_assessment/send", "$actions_dir/send.php");
	// send forms and sets to subgroups to start the peer assessment process
	elgg_register_action("peer_assessment/launch", "$actions_dir/launch.php");
	// send forms and sets to subgroups to start the peer assessment process
	elgg_register_action("peer_assessment/set_access", "$actions_dir/set_access.php");
	// save form state in subgroups 
	elgg_register_action("peer_assessment/save", "$actions_dir/save.php");
	
	
	elgg_register_page_handler('peer_assessment', 'peer_assessment_page_handler');
	
	// add a peer_assessment link to owner blocks
	elgg_register_plugin_hook_handler('register', 'menu:owner_block', 'peer_assessment_owner_block_menu');

	// write permission plugin hooks
	elgg_register_plugin_hook_handler('permissions_check', 'object', 'peer_assessment_write_permission_check');
	elgg_register_plugin_hook_handler('container_permissions_check', 'object', 'peer_assessment_container_permission_check');

	// Add group option
    add_group_tool_option('peer_assessment', elgg_echo('peer_assessment:enable_peer_assessment'), false);
}


function peer_assessment_page_handler($page) {

	// push all breadcrumbs -> link to parent page in group??
	elgg_push_breadcrumb(elgg_echo('peer_assessment:peer_assessment'));

	//url of type -> peer_assessment/group/[guid]/xxxx
	// use value of xxx to know which page to use as we are always in a group
	$page_type = $page[2];
	
	$pages_dir = dirname(__FILE__) . '/pages/peer_assessment';
	switch ($page_type) {
		case 'model':
			set_input('group_guid', $page[1]);
			require_once "$pages_dir/save_model.php";
			break;
		case 'model_view':
			set_input('group_guid', $page[1]);
			require_once "$pages_dir/model.php";
			break;
		case 'list_subgroups':
			set_input('group_guid', $page[1]);
			require_once "$pages_dir/list_subgroups.php";
			break;	
		case 'parent':
			set_input('group_guid', $page[1]);
			require_once "$pages_dir/parent.php";
			break;
		case 'form':
			set_input('group_guid', $page[1]);
			require_once "$pages_dir/save.php";
			break;
		case 'form_view':
			set_input('group_guid', $page[1]);
			require_once "$pages_dir/form.php";
			break;
		default:
			break;
	}
}

/**
 *
 * create menu in group to access peer assessment model creation page
 *
**/
function group_menus_peer_assessment() {

	//pageowner is the group entity here
	$pageowner = elgg_get_page_owner_entity();
	
	
	//add link to peer assessment model creator page
	
	if ($pageowner instanceof ElggGroup && $pageowner->peer_assessment_enable == 'yes') {
				
		$loggedinuser = elgg_get_logged_in_user_guid();
		$isgrpadmin = is_group_admin($pageowner, $loggedinuser);
		
		// add  link to p_a model creator only to grp admins or grp owner
		if($isgrpadmin){
			//add link to p_a model creator 
			$url = "peer_assessment/group/{$pageowner->guid}/parent";
			elgg_register_menu_item('page', new ElggMenuItem('peer_assessment',
			                elgg_echo('peer_assessment:peer_assessment_menu:group_parent'), $url ) );
			                
		}
	}
}


/**
 * Add a menu item to the user ownerblock
 */
function peer_assessment_owner_block_menu($hook, $type, $return, $params) {
	
	$group = $params['entity'];
	
	// if we are in a group and group parent exist and 
	//P-A is enable in this group then display form link to all members
	if (elgg_instanceof($group, 'group') ){
		
		$group_parent = get_group_parent($group->guid, false);

		if (!empty($group_parent) && $group_parent->peer_assessment_enable == "yes") {
			
			$form = get_peer_assessment_entity_by_container($group->guid, 'form_peer_assessment');
			
			if($form->sent != true){
				$url = "peer_assessment/group/{$group->guid}/form";
			}else{
				$url = "peer_assessment/group/{$group->guid}/form_view";
			}
			elgg_register_menu_item('page', new ElggMenuItem('peer_assessment',
				                elgg_echo('peer_assessment:peer_assessment_menu:group'), $url ) );
		} 

	}
}

/**
 * Extend permissions checking to extend can-edit for write users.
 *
 * @param string $hook
 * @param string $entity_type
 * @param bool   $returnvalue
 * @param array  $params
 */
function peer_assessment_write_permission_check($hook, $entity_type, $returnvalue, $params) {
	if ($params['entity']->getSubtype() == 'answer_peer_assessment'
		/*|| $params['entity']->getSubtype() == 'model_peer_assessment' 
			|| $params['entity']->getSubtype() == 'question_peer_assessment'*/) {

		$write_permission = $params['entity']->write_access_id;
		$user = $params['user'];

		if ($write_permission && $user) {
			switch ($write_permission) {
				case ACCESS_PRIVATE:
					// Elgg's default decision is what we want
					return;
					break;
				default:
					$list = get_access_array($user->guid);
					if (in_array($write_permission, $list)) {
						// user in the access collection
						return true;
					}
					break;
			}
		}
	}
}

function peer_assessment_write_handler($hook, $type, $return_value, $params){
	$result = $return_value;
	
	$container_id = elgg_get_page_owner_guid();
	
	if(elgg_instanceof($group, 'group') && !empty($result) && is_array($result)){
		
		
		$collections = get_user_access_collections($container_id);
		if ($collections) {
			foreach ($collections as $collection) {
				$result[$collection->id] =  $collection->name;
			}
		}
		
		// reverse the array
		$result = array_reverse($result, true);
		
	}
	
	return $result;
}


elgg_register_event_handler('init', 'system', 'peer_assessment_init');
elgg_register_event_handler('pagesetup', 'system', 'group_menus_peer_assessment');
